# fast-postgres-deployment

A collection of BASH scripts to prepare and deploy EDB's Postgress Deploy 
(https://github.com/EnterpriseDB/postgres-deployment) 

Please note that a valid subscription of one of EnterpriseDB's plans is required to access EDB applications.

The collection of BASH scripts will use the 'baremetal' methode of deployment, becaus it's applicable to all kind's of setups (VM/Container/CLoud).

Details of the Reference Architecture can be found [here](https://github.com/EnterpriseDB/edb-ref-archs/blob/main/edb-reference-architecture-codes/README.md).:

* EDB-RA-2 - Reference architecture with 1 primary and 2 secundary databases with automatic fail over (EFM) and a backup application (BARMAN)

[other Reference Architectures will be addet soon]


[![MIT License](https://img.shields.io/apm/l/atomic-design-ui.svg?)](https://github.com/tterb/atomic-design-ui/blob/master/LICENSEs)


## Installation: Please see the README.md files for each architecture.
    
