#!/bin/bash
## Script to prepare the instances / server for an automated postgres-deployment
## ( https://github.com/EnterpriseDB/postgres-deployment/blob/edb_pot_v2/README.md )
##------------------------------------------------------------------------------
db1_server='{IP address}'
db2_server='{IP address}'
db3_server='{IP address}'
mon_server='{IP address}'
bak_server='{IP address}'
wks_server='{IP address}'
##------------------------------------------------------------------------------
# install requirements
sudo yum -y update
sudo yum -y upgrade
sudo yum -y install epel-release
sudo yum -y install sudo yum-utils
sudo yum -y install nano git curl htop python3.8 python3-pip gcc python3-devel openssh-clients sshpass hostname
##-----------------------------------------------------------------------------
# configure sudo for paswordless access
sudo sed -i -e 's/# %wheel/%wheel/g' /etc/sudoers
##-----------------------------------------------------------------------------
# configure openSSH for paswordless access
cd  ~/
mkdir ~/.ssh
echo '' > ~/.ssh/authorized_keys
sudo chmod 700 ~/.ssh
sudo chmod 600 ~/.ssh/authorized_keys
 echo '### automated deployment ##' > sshd_config
 echo 'Port 22' >> sshd_config
 echo 'PermitRootLogin yes' >> sshd_config
 echo 'PermitRootLogin without-password' >> sshd_config
 echo 'PubkeyAuthentication yes' >> sshd_config
 echo 'ChallengeResponseAuthentication no' >> sshd_config
 echo 'UsePAM yes' >> sshd_config
 echo 'X11Forwarding yes' >> sshd_config
 echo 'PrintMotd no' >> sshd_config
 echo 'AcceptEnv LANG LC_*' >> sshd_config
 echo 'Subsystem sftp internal-sftp' >> sshd_config
 echo 'TCPKeepAlive yes' >> sshd_config
sudo mv /etc/ssh/sshd_config /etc/ssh/sshd_config.bak
sudo mv sshd_config /etc/ssh/sshd_config
sudo systemctl restart sshd
##------------------------------------------------------------------------------
# create hosts file
  echo '127.0.0.1  localhost localhost.localdomain' > hosts
  echo $db1_server' db1.lan db1' >> hosts
  echo $db2_server' db2.lan db2' >> hosts
  echo $db3_server' db3.lan db3' >> hosts
  echo $mon_server' mon.lan mon' >> hosts
  echo $bak_server' bak.lan bak' >> hosts
  echo $wks_server' wks.lan wks' >> hosts
sudo cp hosts /etc/hosts
##------------------------------------------------------------------------------
