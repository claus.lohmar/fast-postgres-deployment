#!/bin/bash
## Script to prepare the workstation / controler for an automated postgres-deployment
## ( https://github.com/EnterpriseDB/postgres-deployment/blob/edb_pot_v2/README.md )
##------------------------------------------------------------------------------
cd  ~/
## requirements - workstation only
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo
sudo yum -y install terraform
sudo ln -s /bin/python3 /bin/python
python -m pip install --upgrade pip --user
pip install ansible-core==2.11.1 --user
ansible --version
pip install edb-deployment --user
export PATH=$PATH:$HOME/.edb-cloud-tools/bin
eval "$(register-python-argcomplete edb-deployment)"
edb-deployment --version
pip install virtualenv --user
##------------------------------------------------------------------------------
cd  ~/.ssh
ssh-keygen -t rsa
## configure passwordless login
ssh_user='{user}'
export SSHPASS='{secret}'
db1_server='{IP address}'
db2_server='{IP address}'
db3_server='{IP address}'
mon_server='{IP address}'
bak_server='{IP address}'

ssh-keyscan -H $db1_server >> ~/.ssh/known_hosts
ssh-keyscan -H $db2_server >> ~/.ssh/known_hosts
ssh-keyscan -H $db3_server >> ~/.ssh/known_hosts
ssh-keyscan -H $mon_server >> ~/.ssh/known_hosts
ssh-keyscan -H $bak_server >> ~/.ssh/known_hosts

sshpass -e ssh-copy-id $ssh_user@$db1_server
sshpass -e ssh-copy-id $ssh_user@$db2_server
sshpass -e ssh-copy-id $ssh_user@$db3_server
sshpass -e ssh-copy-id $ssh_user@$mon_server
sshpass -e ssh-copy-id $ssh_user@$bak_server

scp id_rsa $ssh_user@$db1_server:.ssh/
scp id_rsa.pub $ssh_user@$db1_server:.ssh/
scp id_rsa $ssh_user@$db2_server:.ssh/
scp id_rsa.pub $ssh_user@$db2_server:.ssh/
scp id_rsa $ssh_user@$db3_server:.ssh/
scp id_rsa.pub $ssh_user@$db3_server:.ssh/
scp id_rsa $ssh_user@$mon_server:.ssh/
scp id_rsa.pub $ssh_user@$mon_server:.ssh/
scp id_rsa $ssh_user@$bak_server:.ssh/
scp id_rsa.pub $ssh_user@$bak_server:.ssh/
# ------------------------------------------------------------------------------
