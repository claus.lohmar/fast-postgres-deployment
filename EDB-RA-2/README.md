# Fast Deploy of EDB-RA-2 architecure

![](./EDB-RA-2.png)

## requirements - software :
All instances are running CentOS 8 ( RHEL / FEDORA may work but are not tested )
Access to the EDB repositories (https://www.enterprisedb.com/accounts/profile) is required.

**requirements - infrastructure :**
- DB1  (postgres_server_1 - min 2 cpu | 8GB ram | 10GB storage)
- DB2  (postgres_server_2 - min 2 cpu | 8GB ram | 10GB storage)
- DB3  (postgres_server_3 - min 2 cpu | 8GB ram | 10GB storage)
- MON  (Monitoring Server 'PEM' - min 2 cpu | 8GB ram | 10GB storage)
- BAK  (backup_server - min 2 cpu | 8GB ram | 10GB storage)     
- WKS  (workstation / controler - min 2 cpu | 8GB ram | 10GB storage)

**Steps:**
  1)  **Edit the setup_server.sh script, providing:**
        * the IP addresses for the instances.

      Copy the script to all server, including the workstation and execute it.
      NOTE:     Ansible executes commands as SUDO user without a password.

  2)  **Edit the setup_workstation.sh script, providing:**
        * the ip addresses for the instances
        * the username for the ssh user
        * the password for the ssh user

      Copy the script to the workstation instance and execute it.

  3)  **Edit the deploy.sh providing:**
        * the ip addresses for the instances
        * the username for the ssh user
        * the EDB repository credentials
      
      Copy the script to the workstation instance and execute it.

## Obs: 
- To build a simple HA architecture, with failover and desaster recovery, place the server DB3 and BAK in a different data center.
- For an advanced HA solution, deploy EDB-RA-2 in two different datacente and then establish replication and fail-over between Datacenter_A and Datacenter_B
- Add PG-POOL or PG_BOUNCER for loadbalancing and read scalability.  
