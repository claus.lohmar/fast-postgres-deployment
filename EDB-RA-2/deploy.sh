#!/bin/bash
## Script to execute the postgres-deployment ( https://github.com/EnterpriseDB/postgres-deployment/blob/edb_pot_v2/README.md )
##------------------------------------------------------------------------------
ssh_user='{user}'
credentials='{username}:{secret}'
db1_public_ip='{IP address}'
db1_private_ip='{IP address}'
db2_public_ip='{IP address}'
db2_private_ip='{IP address}'
db3_public_ip='{IP address}'
db3_private_ip='{IP address}'
mon_public_ip='{IP address}'
mon_private_ip='{IP address}'
bak_public_ip='{IP address}'
bak_private_ip='{IP address}'
##------------------------------------------------------------------------------
# clean up old project
edb-deployment baremetal remove deployment
##------------------------------------------------------------------------------
# Creating the specification file
echo '{
  "ssh_user": "'$ssh_user'",
    "pg_data": null,
    "pg_wal": null,
  "postgres_private_ip_1": {
    "name": "db1",
    "public_ip": "'$db1_public_ip'",
    "private_ip": "'$db1_private_ip'"
  },
  "postgres_private_ip_2": {
    "name": "db2",
    "public_ip": "'$db2_public_ip'",
    "private_ip": "'$db2_private_ip'"
  },
  "postgres_private_ip_3": {
    "name": "db3",
    "public_ip": "'$db3_public_ip'",
    "private_ip":"'$db3_private_ip'"
  },
  "pem_private_ip_1": {
    "name": "mon",
    "public_ip": "'$mon_public_ip'",
    "private_ip": "'$mon_private_ip'"
  },
  "backup_private_ip_1": {
    "name": "bak",
    "public_ip": "'$bak_public_ip'",
    "private_ip": "'$bak_private_ip'"
  }
}'>  baremetal-edb-ra-2.json
##------------------------------------------------------------------------------
# Prepariong the deployment
edb-deployment baremetal configure deployment \
--reference-architecture EDB-RA-2 \
--edb-credentials $credentials \
--pg-type PG \
--pg-version 13 \
--efm-version 4.2 \
--ssh-private-key ~/.ssh/id_rsa \
--spec baremetal-edb-ra-2.json \
##------------------------------------------------------------------------------
# executing the deployment
edb-deployment baremetal deploy deployment
